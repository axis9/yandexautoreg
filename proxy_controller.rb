# подключение базы
require_relative "sqlite.rb"
require_relative "curb_helper.rb"
require 'json'

wipe_used = true
wipe_proxy = true
update_used = true

db = ProxyController.new

db.wipe_used if wipe_used
db.wipe_proxy if wipe_proxy
db.update_used if update_used

result = request url: "http://box.directon.ru/ss/addsocks.php?all"
JSON.parse(result.body_str).each do |proxy| 
	p = proxy["ip"]+":"+proxy["port"]
	puts p
	db.insert_proxy p
end