require 'net/http'
require 'net/https'
require 'socksify/http'
# подключение базы
require_relative "sqlite.rb"

url = 'https://socks5proxies.com/'
username = 'deconf@ya.ru'
passwd = 'netisnet'
$session = ''

wipe_used = true
wipe_proxy = true
update_used = true

db = ProxyController.new

db.wipe_used if wipe_used
db.wipe_proxy if wipe_proxy
db.update_used if update_used

def request(options = {})
	begin
		options[:url] ||=  'https://socks5proxies.com/'
		options[:get] ||= {}
		options[:post] ||= false
		options[:https] ||= false
		options[:current_session] ||= nil
		uri = URI(options[:url])
		uri.query = URI.encode_www_form(options[:get]) unless options[:get].empty?
		http = Net::HTTP.new(uri.host, uri.port)
		if options[:https]
			http.use_ssl = true
			http.verify_mode = OpenSSL::SSL::VERIFY_NONE
		end
		if options[:post]
			request = Net::HTTP::Post.new(uri.request_uri)
			request.set_form_data(options[:post])
		else
			request = Net::HTTP::Get.new(uri.request_uri)
		end
		request['Cookie'] = $session if options[:current_session]
		request['User-Agent'] = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:16.0) Gecko/20100101 Firefox/16.0'
		request['Connection'] = 'keep-alive'
		request['Accept'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
		http.request(request)
	rescue Exception => msg
		nil
	end
end

def get_proxy(html)
	html.scan /\<div id=\'(\d+.\d+.\d+.\d+\:\d+)\'/
end

def write_in_file(data)
	aFile = File.new('test.html', "w")
	aFile.write(data)
	aFile.close
end

result = request https: true

custom_cookies = []
result.get_fields('set-cookie').each do |cookie|
	custom_cookies << cookie.split(';')[0]
end

$session = custom_cookies.join '; '
result = request url: 'https://socks5proxies.com/index.php?action=memberlogin', 
				post: { email: username, password: passwd, login: 'Login' },
				current_session: true,
				https: true

i=1
loop do
	result = request url: 'https://socks5proxies.com/index.php?action=viewsocks&page='+i.to_s, current_session: true, https: true
	break if result.body.include? 'No Record Found'
	get_proxy(result.body).each do |prx| 
		# puts prx
		begin
			proxy = prx[0].split ':'
			uri = URI('http://checkip.dyndns.org/')
			http = Net::HTTP::SOCKSProxy(proxy[0], proxy[1]).new(uri.host)
			request = Net::HTTP::Get.new(uri.request_uri)
			checker = http.request(request)
			rescue Exception => msg
				# p prx[0] + ' -> '+ msg.to_s
		end
		puts prx[0] if checker.instance_of? Net::HTTPOK
		db.insert_proxy prx[0] if checker.instance_of? Net::HTTPOK
	end
	i+=1
end