# coding: utf-8
# гейт между ruby и ff
require "selenium-webdriver"
# работа с JSON
require "json"
#рабора с датами
require "date"

class YandexSelenium
	attr_reader :driver
	attr_reader :proxy
	attr_reader :first_name, :last_name, :password, :secret_answer, :api, :user_agent, :ya_cookies
	attr_accessor :nickname

	def initialize(options = {})
		# создаем профиль
		profile = Selenium::WebDriver::Firefox::Profile.new
		
		options[:ip] ||= nil
		options[:port] ||= nil
		options[:user_agent] ||= nil
		options[:cookies] ||= nil

		@ya_cookies = options[:cookies]

		@proxy = { ip: options[:ip], port: options[:port] }

		profile['network.proxy.http'] = options[:ip]
		profile['network.proxy.http_port'] = options[:port].to_i
		profile['network.proxy.type'] = 1

		if options[:user_agent].nil?
			@user_agent = rand_user_agent
			profile['general.useragent.override'] = @user_agent
		else
			@user_agent = options[:user_agent]
			profile['general.useragent.override'] = options[:user_agent]
		end

		puts "User agent #{@user_agent}"
		puts "Browser get proxy #{@proxy[:ip]}:#{@proxy[:port]}"

		@api = false

		# подгружаем профиль в ff
		begin
			@driver = Selenium::WebDriver.for :firefox, :profile => profile
			# таймаут на поиск элементов
			@driver.manage.timeouts.implicit_wait = 120
			# гасим куки
			@driver.manage.delete_all_cookies
		rescue Exception => msg
			puts msg
			@driver = nil
		end
	end

	def goto(url, cookies=true)
		begin
			@driver.navigate.to url
			if cookies
				@driver.manage.delete_all_cookies
				# добавляем куки, если они есть во входящих параметрах конструктора экземпляра класса
				unless @ya_cookies.nil?
					@ya_cookies.split(';').each do |c|
						cookie_name, cookie_value, cookie_domain, cookie_date = c.split('@',4)

						# DateTime.parse "2015-11-28T09:48:06+00:00"
						# 10 years - yandexuid
						# 26 years - yp
						# 3 months - sessionid2
						# 3 years - L
						# puts "-#{cookie_date}-#{cookie_date.empty?}-"
						# puts cookie_date
						unless cookie_date.empty?
							@driver.manage.add_cookie(name: cookie_name, value: cookie_value, expires: DateTime.parse(cookie_date).to_time.to_i)
						# else?
							# @driver.manage.add_cookie(name: cookie_name, value: cookie_value)
						end

						# puts "Added cookie: #{cookie_name} => #{cookie_value} => #{cookie_date}"
					end
				end
			end
		rescue Exception => msg
			puts "Cant go to URL %s" % [msg]
			return nil
		end
		true
	end

	def send_key(type, find_params, value)
		begin
			element = @driver.find_element(type, find_params)
			element.send_keys value
		rescue Exception => msg
			puts "Cant find element! [%s -> %s -> %s] = %s" % [type, find_params, value, msg]
			nil
		end
		true
	end

	def choose_cat
		puts "Try choose cat theme"
		@driver.execute_script('$(".b-theme-teaser__cover").click()')
		puts "End choose cat theme"
	end

	def text_from_filed(type, arg)
		begin
			element = @driver.find_element(type, arg)
			result = element.attribute('value');
		rescue Exception => msg
			puts "Cant find element! [%s -> %s] = %s" % [type, arg, msg]
			nil
		end
		result
	end

	def fill_by_name(find_hash)
		fail_flag = true
		find_hash.each_pair do |key, value|
			result = send_key(:name, key, value)
			fail_flag = false if result.nil?
		end
		return fail_flag
	end

	def generate_password(size = 9)
		chars = [('a'..'z').to_a, ('0'..'9').to_a, ('A'..'Z').to_a, ['!','@','#','$','%','^','&','*','(',')','_','-','+',':',';',',','.']]
		switcher = -1
		@password = (1..size).collect do |a|
			switcher+=1
			switcher = 0 if switcher > 3
			chars[switcher][rand(chars[switcher].size)]
		end.join
		@password
	end

	def choose_dropdown(name, value)
		choosed_flag = false
		begin
			element = @driver.find_element(name: name)
			options = element.find_elements(:tag_name=>"option")
			options.each do |el|
				if el.attribute('value').eql? value
					el.click
					choosed_flag = true
				end
			end
		rescue Exception => msg
			puts "Cant choose value #{value} from #{name} -> #{msg}"
			return nil
		end
		choosed_flag
	end

	def captcha_answer
		get_capcha_string = %{var img = document.getElementById('captcha-img');
		var canvas = document.createElement("canvas");
		canvas.width = img.width;
		canvas.height = img.height;
		var ctx = canvas.getContext("2d");
		ctx.drawImage(img, 0, 0);
		return canvas.toDataURL();}

		antigate = Antigate.new
		captcha = nil
		begin
			captcha = antigate.fill_check @driver.execute_script(get_capcha_string)
		rescue
			puts "### Cant load captcha"
		end

		captcha = "НЕВИЖУ" if captcha.nil?
		captcha
	end

	def direct_login(options = {})
		options[:login] ||= @nickname+"@yandex.ru"
		options[:password] ||= @password
		begin
			
			result = goto 'http://browserspy.dk/browser.php'
			
			exit
			if result.nil?
				puts "Proxy %s:%s died" % [proxy_ip, proxy_port]
				begin
					quit
				rescue
					puts "Browser not loaded"
					return nil
				end
			end

			result = fill_by_name({login: options[:login], passwd: options[:password]})

			return nil if result.eql? false

			result = submit 'login'

			return nil if result.nil? false
		rescue Exception => msg
			puts msg
			return nil
		end
		true
	end

	def yandex_random_secret_answer
		secret_answer = {
			'1' => ['Девичья фамилия матери', NameGen.last_name(:female)],
			'3' => ['Почтовый индекс родителей', /\d{6}/.gen],
			'4' => ['Дата рождения бабушки', Date.new(1930+rand(20), rand(11)+1, rand(27)+1).to_s],
			'6' => ['Номер паспорта',/\d{6}/.gen],
			'7' => ['Пять последних цифр кред. карты',/\d{5}/.gen],
			'8' => ['Пять последних цифр ИНН',/\d{5}/.gen],
			'9' => ['Ваш любимый номер телефона',/9\d{2}-\d{3}-\d{4}/.gen]
		}
		hintq = secret_answer.keys.sample
		hinta = secret_answer[hintq][1]
		@secret_answer = hinta

		# выбираем из списка dropdown 
		result = choose_dropdown "hintq", hintq
		if result.nil?
			puts "#### Fail choose drop down secret answer"
			return nil
		end

		result = fill_by_name({ hinta: hinta })
		if result.nil?
			puts "#### Fail input secret answer"
			return nil
		end
		hinta
	end

	def click(type, arg, options={})
		options[:index] ||= nil
		begin
			if options[:index].nil?
				@driver.find_element(type, arg).click
			else
				index = 1
				@driver.find_elements(type, arg) do |element|
					if options[:index].eql? index
						element.click
					end
					index+=1
				end
			end
		rescue Exception => msg
			puts "Cant click on #{type} -> #{arg} -> #{msg}"
			return nil
		end
		true
	end

	def human_emulation
		#начинаем пользоваться
		begin
			@driver.find_element(:link_text, "Начать пользоваться").click

			# гасим визард
			@driver.find_element(:link_text, "Позже").click

			# ждем закрытия
			sleep 5

			#кликаем на чекбоксах для выбора писем
			@driver.find_elements(:class, "b-messages__message__checkbox__input").each do |element|
				element.click
			end

			# удаляем письма	
			@driver.find_element(:link_text, "Удалить").click

			# выбираю тему
			choose_cat
			
			# говорю что не надо подключать ящики
			@driver.find_element(:link_text, "у меня нет других ящиков").click
			
		rescue Exception => msg
			puts msg
			return false
		end
		true
	end

	def check_captcha
		puts "Check captcha"
		begin
			element = @driver.find_element(:link_text, "Начать пользоваться")
		rescue
			return false
		end
		true
	end

	def get_idkey
		idkey = nil
		begin
			idkey = @driver.execute_script("return Passport.idkey;")
		rescue
			return nil
		end
		idkey
	end

	def unique_reqid
		100000+(rand * 899999).floor
	end

	def recommended_name(first_name, last_name, nickname)
		idkey = get_idkey
		return nil if idkey.nil?

		@first_name = first_name
		@last_name = last_name
		recomment_counter = 0
		result = ''

		loop do
			result = request url:'https://passport.yandex.ru/passport', get: { mode: 'constructlogin', iname: first_name, fname: last_name, lang: 'ru', idkey: idkey, reqid: unique_reqid, login: nickname }, proxy: "#{@proxy[:ip]}:#{@proxy[:port]}", proxy_pw:"77a517a4fe184729:ea073b80df3f95e1", proxy_type: Curl::CURLPROXY_HTTP, cookie: get_cookies, user_agent: @user_agent
			break unless result.nil?
			recomment_counter += 1
			return nil if recomment_counter>5
		end

		parsed = nil
		nickname = nil

		begin
			parsed = JSON.parse(result.body_str)
			nickname = parsed['logins'].sample
		rescue
			puts "Cant get nickname: %s -> %s" % [parsed, nickname]
			return nil
		end
		@nickname = nickname
		nickname
	end

	def submit(field_name)
		begin
			element = @driver.find_element(:name, field_name)
			element.submit
		rescue Exception => msg
			puts "Cant submit form from field #{field_name} -> #{msg}"
			return nil
		end
		true
	end

	def get_cookies(full=nil)
		send_cookies = ""
		# puts "@@@@@@@@@@@@@@@@@@@@@"
		# p @driver.manage.all_cookies
		# puts "@@@@@@@@@@@@@@@@@@@@@"
		@driver.manage.all_cookies.each do |cook|
			send_cookies << "%s=%s;" % [cook[:name], cook[:value]] unless full
			send_cookies << "%s@%s@%s@%s;" % [cook[:name], cook[:value], cook[:domain], cook[:expires]] if full
		end
		send_cookies
	end

	def quit
		@driver.quit
	end

	def rand_user_agent
		user_agent = ['Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.13 (KHTML, like Gecko) Chrome/24.0.1290.1 Safari/537.13',
		'Mozilla/5.0 (Windows NT 6.2) AppleWebKit/537.13 (KHTML, like Gecko) Chrome/24.0.1290.1 Safari/537.13',
		'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.13 (KHTML, like Gecko) Chrome/24.0.1284.0 Safari/537.13',
		'Mozilla/5.0 (Windows NT 6.2) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.26 Safari/537.11',
		'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.17 Safari/537.11',
		'Mozilla/5.0 (Windows NT 6.2) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4',
		'Opera/12.80 (Windows NT 5.1; U; en) Presto/2.10.289 Version/12.02',
		'Opera/9.80 (Windows NT 6.1; U; es-ES) Presto/2.9.181 Version/12.00',
		'Opera/9.80 (Windows NT 5.1; U; zh-sg) Presto/2.9.181 Version/12.00',
		'Opera/12.0(Windows NT 5.2;U;en)Presto/22.9.168 Version/12.00',
		'Opera/12.0(Windows NT 5.1;U;en)Presto/22.9.168 Version/12.00',
		'Mozilla/5.0 (Windows NT 5.1) Gecko/20100101 Firefox/14.0 Opera/12.0',
		'Opera/9.80 (Windows NT 6.1; WOW64; U; pt) Presto/2.10.229 Version/11.62',
		'Opera/9.80 (Windows NT 6.0; U; pl) Presto/2.10.229 Version/11.62',
		'Opera/9.80 (Windows NT 5.1; U; en) Presto/2.9.168 Version/11.51',
		'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; de) Opera 11.51',
		'Opera/9.80 (X11; Linux x86_64; U; fr) Presto/2.9.168 Version/11.50',
		'Opera/9.80 (X11; Linux i686; U; hu) Presto/2.9.168 Version/11.50',
		'Opera/9.80 (X11; Linux i686; U; ru) Presto/2.8.131 Version/11.11',
		'Opera/9.80 (X11; Linux i686; U; es-ES) Presto/2.8.131 Version/11.11',
		'Mozilla/5.0 (Windows NT 5.1; U; en; rv:1.8.1) Gecko/20061208 Firefox/5.0 Opera 11.11',
		'Opera/9.80 (X11; Linux x86_64; U; bg) Presto/2.8.131 Version/11.10',
		'Opera/9.80 (Windows NT 6.0; U; en) Presto/2.8.99 Version/11.10',
		'Opera/9.80 (Windows NT 5.1; U; zh-tw) Presto/2.8.131 Version/11.10',
		'Opera/9.80 (Windows NT 6.1; Opera Tablet/15165; U; en) Presto/2.8.149 Version/11.1',
		'Mozilla/6.0 (Windows NT 6.2; WOW64; rv:16.0.1) Gecko/20121011 Firefox/16.0.1',
		'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:16.0.1) Gecko/20121011 Firefox/16.0.1',
		'Mozilla/5.0 (Windows NT 6.2; Win64; x64; rv:16.0.1) Gecko/20121011 Firefox/16.0.1',
		'Mozilla/5.0 (Windows NT 6.1; rv:15.0) Gecko/20120716 Firefox/15.0a2',
		'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.1.16) Gecko/20120427 Firefox/15.0a1',
		'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20120427 Firefox/15.0a1',
		'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:15.0) Gecko/20120910144328 Firefox/15.0.2',
		'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:15.0) Gecko/20100101 Firefox/15.0.1']
		user_agent.sample
	end
end