# coding: utf-8
# сторонняя либа для подключения antigate

require_relative "antigate"
# selenium helper
require_relative "selenium_helper"
# curb helper
require_relative "curb_helper"
# генератор никнеймов
require 'randexp'
require 'csv'
# подключение базы
require_relative "sqlite.rb"

def save_file(str)
	aFile = File.new('yandex_accounts', "a")
	aFile.write(Time.now.to_s + "|" + str + "\n")
	aFile.close
end

NEED_API = $*[0]=='api' ? true : nil

db = ProxyController.new
CSV.foreach("yandex_accounts") do |row|
	date, username, password, first_name, last_name, answer, proxy, api_access, cookies, user_agent = row[0].split '|'
	
	puts "Using %s %s" % [username, password]

	puts "Get proxy from base"
	# proxy = db.get_proxy
	proxy = proxy
	# proxy = '94.168.167.160:1954'
	if proxy.nil?
		puts "Proxy base is empty. Sleep 2 minutes"
		sleep 120
		next
	end

	proxy_ip, proxy_port = proxy.split ':'

	# начальные данные
	# прокси
	puts "Use proxy %s:%s" % [proxy_ip, proxy_port]

	driver = YandexSelenium.new(ip: proxy_ip, port: proxy_port, user_agent: user_agent, cookies: cookies)

	# открываем страничку
	result = driver.direct_login(login: username, password: password)

	# если не открылась, то гасим
	if result.nil?
		exit
		puts "Proxy %s:%s died" % [proxy_ip, proxy_port]
		begin
			driver.quit
		rescue
			puts "Browser not loaded"
		end
		next
	end
	
	driver.quit
end
