# coding: utf-8

# http://box.directon.ru/ss/addsocks.php

# сторонняя либа для подключения antigate
require_relative "antigate"
# selenium helper
require_relative "selenium_helper"
# curb helper
require_relative "curb_helper"
# подключение базы
require_relative "sqlite.rb"
# генератор имен
require_relative "name_generate.rb"

def save_file(str)
	aFile = File.new('yandex_accounts', "a")
	aFile.write(Time.now.to_s + "|" + str + "\n")
	aFile.close
end

NEED_API = $*[0]=='api' ? true : nil

db = ProxyController.new
# nickname generated masks

loop do
	puts "Get proxy from base"
	# proxy = db.get_proxy
	proxy = "199.116.85.163:3131"
	
	if proxy.nil?
		puts "Proxy base is empty. Sleep 2 minutes"
		sleep 120
		next
	end

	prx = proxy.split ':'

	proxy_ip = prx[0]
	proxy_port = prx[1]

	# начальные данные
	# прокси, имя, фамилия, никнейм

	# рандомно выбираем пол 
	gender = [:male, :female][rand(2)]
	# генерим имя, фамилию, никнейм
	first_name_str = NameGen.first_name(gender)
	last_name_str = NameGen.last_name(gender)
	# nickname = NameGen.nickname

	puts "Use proxy %s:%s" % [proxy_ip, proxy_port]

	driver = YandexSelenium.new(ip: proxy_ip, port: proxy_port)

	# открываем главную страницу
	result = driver.goto "http://yandex.ru"

	if result.nil?
		puts "Proxy %s:%s died" % [proxy_ip, proxy_port]
		begin
			driver.quit
		rescue
			puts "Browser not loaded"
		end
		next
	end

	result = driver.goto "https://passport.yandex.ru/passport?mode=register&msg=mail&retpath=https://mail.yandex.ru/promo/new/registered/quickdeliver"

	if result.nil?
		puts "Proxy %s:%s died" % [proxy_ip, proxy_port]
		begin
			driver.quit
		rescue
			puts "Browser not loaded"
		end
		next
	end

	# кликаем на странице "завести почту"
	# find_button = false

	# ["b-form-button__input", "b-mail__promo__button,b-mail__promo__button_lang_RU"].each do |click_class|
	# 	result = driver.click(:class, click_class)
	# 	unless result.nil?
	# 		find_button = true
	# 		break
	# 	end
	# end

	# unless find_button
	# 	puts "#### Cant find Register button"
	# 	begin
	# 		driver.quit
	# 	rescue
	# 		puts "Browser not loaded"
	# 	end
	# 	next
	# end
	# exit

	# получаем рекомендованное имя
	# recommended_nickname = driver.recommended_name(first_name_str, last_name_str, nickname)
	# гасим если не получили

	# if recommended_nickname.nil?
	# 	puts "#### Fail get recommended name"
	# 	driver.quit
	# 	next
	# end
	# вбиваем поля
	result = driver.fill_by_name({ iname: first_name_str, fname: last_name_str })
	if result.eql? false
		puts "#### Fail fill first name, last name"
		next
	end

	result = driver.click(:class, "dot")
	
	# гасим если не получилось заполнить формы
	if result.eql? nil
		puts "#### Fail choose nickname"
		next
	end

	recommended_nickname = driver.text_from_filed(:name, "login").to_s
	driver.nickname = recommended_nickname
	puts "Name: %s %s" % [first_name_str, last_name_str]
	puts "Username: %s" % recommended_nickname

	# сабмитим форму
	result = driver.submit :login
	# если не вышло (прокся сдохла), то гасим
	if result.nil?
		puts "#### Fail submit first page"
		driver.quit
		next
	end

	# вторая страница

	# генерим пароль, ответ на секретный вопрос и выводим на экран
	password = driver.generate_password(12)
	puts "Password: %s" % [password]
	answer = driver.yandex_random_secret_answer

	if answer.nil?
		puts "#### Fail set secren answer"
	end

	puts "Secret password answer: %s" % [answer]

	next_flag = false
	
	# бесконечный цикл по вбиванию каптчи и ее проверки
	loop do
		result = driver.fill_by_name({ passwd: password, passwd2: password })

		if result.nil?
			puts "#### Fail fill fields password, password2"
			next
		end

		captcha = driver.captcha_answer

		result = driver.fill_by_name({ code: captcha })

		if result.nil?
			puts "#### Fail fill fields captcha"
			next
		end

		result = driver.submit "code"
		if result.nil?
			driver.quit
			puts "#### Fail submit captcha"
			next_flag = true
			break
		end

		if driver.check_captcha.eql? false
			puts "#### Incorrect captcha"
		else
			puts "Captcha OK"
			break
		end
	end

	next if next_flag

	save_file("%s|%s|%s|%s|%s|%s|%s|%s|%s" % [driver.nickname+"@yandex.ru", driver.password, driver.first_name, driver.last_name, driver.secret_answer,driver.proxy[:ip]+":"+driver.proxy[:port].to_s, driver.api, driver.get_cookies(true), driver.user_agent])

	if driver.human_emulation.eql? false
		puts "#### Human emulation not do!"
	end

	# driver.quit
	exit
end