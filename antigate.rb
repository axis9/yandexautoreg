# coding: utf-8
require 'base64'
class Antigate
	attr_reader :api_key
	def initialize
		@api_key = 'a2af4a128ca3a8474557129ebe20d977'
	end

	def send(base64)
		captcha_file_name = (0...15).map{65.+(rand(26)).chr}.join
		
		if File.exists? captcha_file_name
			File.delete captcha_file_name
		end

		open(captcha_file_name, 'w') do |f| 
			f << Base64.decode64(base64.split('base64,')[1])
		end

		result = request(url:'http://antigate.com/in.php', file: captcha_file_name, post: { method: 'post', key: 'a2af4a128ca3a8474557129ebe20d977', is_russian: 1, regsense: 1}).body_str.split '|'
		return [result[1], nil] if result[0].eql? 'OK'
		[nil,result[0]]
	end

	def check(id)
		result = request(url:'http://antigate.com/res.php', get: { key: 'a2af4a128ca3a8474557129ebe20d977', action: 'get', id: id}).body_str.split('|')
		return [result[1].force_encoding('UTF-8'), nil] if result[0].eql? 'OK'
		[nil, result[0]]
	end

	def fill_check(get_capcha_string)
		iteration = 0
		captcha = nil
		loop do
			puts 'send to antigate'
			antigate_id, error = send get_capcha_string
			unless antigate_id.nil?
				puts 'Sleep before try receive'
				sleep 30
				puts 'Try receive'
				receive_iteration = 0
				loop do
					puts 'Receive '+receive_iteration.to_s
					result, error = check antigate_id
					unless result.nil?
						return result
					else
						puts "Receive error: #{error}"
						if error.eql? 'CAPCHA_NOT_READY'
							puts 'Sleep 45 seconds'
							sleep 45
						else
							puts 'Unknow error, reload captcha'
							break
						end
					end
					receive_iteration += 1
					break if receive_iteration > 10
				end
			else
				puts "Antigate send captcha error: %s" % [error]
			end
			iteration+=1
			break if iteration>3
		end
		nil
	end
end
