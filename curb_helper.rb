# гем Curl для отправки сторонних post запросов
require "curb"

# отправлялка запросов - применяется в antigate и выборки username
def request(options = {})
	begin
		options[:url] ||=  'https://passport.yandex.ru/passport'
		options[:get] ||= {}
		options[:post] ||= false
		options[:https] ||= false
		options[:current_session] ||= nil
		options[:follow_location] ||= false
		options[:cookie] ||= nil
		options[:proxy] ||= nil
		options[:proxy_type] ||= Curl::CURLPROXY_SOCKS4
		options[:proxy_pw] ||= nil
		options[:file] ||= nil
		options[:user_agent] ||= 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:16.0) Gecko/20100101 Firefox/16.0'

		# p options[:url]
		url = options[:url]
		unless options[:get].empty?
			url = options[:url] + '?' + URI.escape(options[:get].collect{|k,v| "#{k}=#{v}"}.join('&'))
		end
		# puts "Proxy: #{options[:proxy]}"

		# p url
		c = Curl::Easy.new(url)
		c.headers["User-Agent"] = options[:user_agent]
		c.headers['Cookie'] = options[:cookie]
		c.headers['Referer'] = 'http://www.yandex.ru/'
		c.headers['Connection'] = 'keep-alive'
		c.headers['Accept'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
      	c.headers['Accept-Language'] = 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3'
      	c.timeout = 30

      	unless options[:proxy].nil?
      		# p "UNDER PROXY %s -> URL %s " % [options[:proxy], url]
	      	c.proxy_url = options[:proxy]
			c.proxy_type = options[:proxy_type]
			c.proxypwd = options[:proxy_pw] unless options[:proxy_pw].nil?
		end
      	
		c.ssl_verify_peer = false
		c.ssl_verify_host = false

		c.verbose = false
		c.follow_location = options[:follow_location]
		c.enable_cookies = true

		if options[:post]
			post_data = options[:post].map { |k, v| Curl::PostField.content(k.to_s, v.to_s) }
			unless options[:file].nil?
				c.multipart_form_post = true
				post_data << Curl::PostField.file('file', options[:file])
			end

			result = c.http_post(post_data)
		else
			result = c.http_get
		end
		return c if result
		nil
	rescue Exception => msg
		p msg
		nil
	end
end