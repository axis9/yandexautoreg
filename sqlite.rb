require 'sqlite3'

class DB
	attr_reader :db_sqlite

	def initialize
		@db_sqlite = SQLite3::Database.new 'proxy.db'
		@db_sqlite.results_as_hash = true
	end

	def insert(table, args)
		@db_sqlite.execute("insert into #{table} values (?"+(',?' *(args.size-1))+")",args)
	end

	def get_max(table)
		@db_sqlite.get_first_row("select max(id)+1 as cnt from #{table}")['cnt']
	end

	def raw(sql)
		@db_sqlite.execute(sql)
	end

	def select(table, where=nil)
		sql = "select * from #{table}"
		sql << " where #{where}" unless where.nil?
		@db_sqlite.execute sql
	end

	def update(table, where=nil, args)
		sql = "update #{table} set "
		sql << args.map do |key, value| 
			if value.instance_of? String
				"%s='%s'" % [key, value]
			else
				"%s=%s" % [key, value]
			end
		end.join(', ') if args.size > 0
		sql << " where #{where}" unless where.nil?
		@db_sqlite.execute sql
	end

	def select(table, where=nil)
		sql = "select * from #{table}"
		sql << " where #{where}" unless where.nil?
		@db_sqlite.execute sql
	end

	def raw_one(sql)
		@db_sqlite.get_first_row(sql)
	end
end

class ProxyController
	attr_reader :db

	def initialize
		@db = DB.new
	end

	def get_proxy
		proxy = @db.raw_one("select p.id, p.proxy from proxy p where p.proxy not in (select cp.proxy from used_proxy cp)")
		return nil if proxy.nil?
		@db.insert('used_proxy', [@db.get_max('used_proxy'), proxy['proxy']])
		proxy['proxy']
	end

	def insert_proxy(proxy)
		@db.insert('proxy', [@db.get_max('proxy'), proxy])
	end

	def wipe_proxy
		@db.raw 'delete from proxy'
	end

	def wipe_used
		@db.raw 'delete from used_proxy'
	end

	def update_used
		puts "Update used proxy"
		File.open("yandex_accounts", "r").each_line do |line|
			time, account, password, first_name, last_name, secret_answer, proxy, api_access, cookies, user_agent = line.split '|'
			puts "Updated: %s" % [proxy]
			@db.insert('used_proxy', [@db.get_max('used_proxy'), proxy])
		end
	end		
end