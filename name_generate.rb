# coding: utf-8
require_relative "curb_helper"
# генератор никнеймов
require 'randexp'
#работа с датами
require 'date'

class NameGen
	def self.first_name(gender = :male)
		lib = { male: 'ru_male', female: 'ru_female' }
		rand_from_file lib[gender]
	end

	def self.last_name(gender = :male)
		fam = get_fam
		return fam if gender.eql? :male

		last_two_chars = fam[-2..-1]
		removed_two_chars = fam[0..-3]
		return fam + "а" if %w{ов ев ин ын}.include? last_two_chars
		return removed_two_chars + "ая" if %w{ий ый ой}.include? last_two_chars
		fam
	end

	def self.rand_from_file(filename)
		File.readlines(filename).sample.strip
	end

	def self.get_fam
		rand_from_file 'ru_fams'
	end

	def self.nickname
		nickname_masks = [/[:word:]{3,6}(-|.)[:word:]{2,7}/,
			/[:word:]{2,4}\d{1,3}[:word:]{3,7}/,
			/\d{2}(-|.)[:word:]{4,10}/,
			/[:word:]{5,9}\d{2,5}/,
			/[:word:]{3}.[:word:]{3}/,
			/[:word:]{2}\d{1}[:word:]{2}\d{2}/,
			/[:word:].[:word:]/,
			/[:word:]{2,7}(-|.)[:word:]{3,5}/,
			/[:word:]{2,5}(-|.)[:word:]{3,6}/,
			/[:word:]{2}\d{3}[:word:]{4}/,
			/[:word:]{4}-[:word:]{2}-\d{3}/
		]
		nickname_masks.sample.gen
	end

	def self.download_fam
		aFile = File.new('russian_fams', "a")
		30.times do |litera|
			next if litera == 0
			puts "Litera #{litera}"
			page = 1
			loop do
				puts "Litera #{litera} -> page #{page}"
				result = request(url: "http://www.edudic.ru/fam/l/#{litera}/p/#{page}").body_str.encode("UTF-8","KOI8-R")
				next if result.include? "Service Temporary"
				fams = result.scan /<a href=\"\/fam\/\d+\/\">(.*?)<\/a>/
				p fams
				break if fams.size.eql? 0
				fams.each{ |fam| aFile.write(fam[0] + "\n") }
				page+=1
				sleep 1
			end
		end
		aFile.close
	end
end